# 安装说明 

* chat_server 是使用了workerman实现的服务端，直接点击start_for_win.bat 启动即可，没有在linux下面测
* chat_client 是前端，基于vue实现

### 服务器端配置
* 我使用的是win10的环境，本地要安装mysql和redis，
* 导入数据库，然后配置连接，数据库配置都在Event.php那个文件里面
* chat_server缺库的话就使用composer安装 
* window系统 直接双击运行start_for_win.bat
* linux系统 执行php start.php start 或 php start.php start -d 

### 客户端配置
* 修改 chat_client\src\api\client.js中的第49行ws地址为你自己的内网地址或域名。
* chat_client缺库的话就使用npm安装
* npm run dev 后访问8080端口看效果
* 就这些了，很简单


## 功能说明

* 群聊
* 登陆、退出登陆
* 掉线重连
* 登陆和掉线重连后获取离线消息
* 超过设定时间自动退出登陆
* 消息持久化

# 效果图

### 登陆/注册界面：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0501/085853_a14892e9_593571.png "1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0501/085904_6a45402d_593571.png "4.png")


### 聊天界面：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0501/085916_54a8effd_593571.png "3.png")

### 群信息界面：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0501/085924_7924d999_593571.png "2.png")
